const {sequelize, Actor, Movie, MovieActor} = require('./models');
const express = require('express');

const app = express();
app.use(express.json());

app.post("/movie", async (req, res) => {
    const {name} = req.body;
    try {
      const movie = await Movie.create({name});
      return res.json(movie);
    } catch (err) {
      return res.json(err);
    }
});

  app.post('/actor', async (req, res) => {

    const {name} = req.body;
    try {
      const actor = await Actor.create({name});
      return res.json(actor);
    } catch (err) {
      return res.json(err);
    }
  })

  app.post('/movieactor', async (req, res) => {

    const {movieId, actorId} = req.body;
    try {
      const movieActor = await MovieActor.create({movieId, actorId});
      return res.json(movieActor);
    } catch (err) {
      return res.json(err);
    }
  })

//   app.get("/movieactor", async (req, res) => {
//     try {
//       const movieActor = await MovieActor.findAll({
//         attributes: ['id'],
//         include: [{
//           model: Movie,
//           attributes: ['name'],
//         }, {
//           model: Actor,
//           attributes: ['name'],
//         }],
//       });
//       return res.json(movieActor);
//     } catch (err) {
//       return res.json(err);
//     }
//   });

app.get("/movieactor", async (req, res) => {
    try {
      const movieActor = await MovieActor.findAll({
        include: [Movie, Actor],
      });
      return res.json(movieActor);
    } catch (err) {
      return res.json(err);
    }
  });

app.listen({port: 8080}, async () => {
    console.log('Server hosted on localhost:5000');
    await sequelize.authenticate();
    console.log("Databse Connected");
})